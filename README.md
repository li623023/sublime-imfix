# sublime-imfix

修复ubuntu下sublime不能输入中文的问题，适合`deb`方式安装的sublime

方法是网上找的，只是写了个脚本，方便安装

# Usage

```shell
git clone https://git.oschina.net/javey/sublime-imfix.git
cd sublime-imfix
sudo ./install.sh
```
