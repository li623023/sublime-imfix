#########################################################################
# File Name: install.sh
# Author: Javey
# mail: jiawei23716@sina.com
# Created Time: 2015年04月30日 星期四 14时08分23秒
#########################################################################
#!/bin/bash

UID=`id -u $USER`

if [ $UID -ne 0 ]; then
    echo "Please run the script by root."
    exit
fi


so_path="/opt/sublime_text/libsublime-imfix.so"
cp ./libsublime-imfix.so $so_path
sed -i "/^Exec=bash/! s|^\(Exec=\)\([^ ]*\)\(.*\)|\1bash -c \"LD_PRELOAD=$so_path \2\3\"|" /usr/share/applications/sublime_text.desktop
sed -i "s|^exec|LD_PRELOAD=$so_path &|" /usr/bin/subl

echo 'Done.'
